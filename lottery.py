
import random
import time

def display(arr, sort=False, delimeter=' - '):
    arr = sorted(arr) if sort else arr
    return delimeter.join(arr)

PAUSE = 1
PAUSE_DRAMATIC = 3

class Lottery():
    bubble = []
    pulls = []

    def __init__(self, items=[], item_multiplier=1, simulation=False):
        self.items = items
        self.item_multiplier = item_multiplier
        self.simulation = simulation

        for n in range(self.item_multiplier):
            self.bubble += self.items

    def mix(self, cycles=7):
        for n in range(cycles):
            random.shuffle(self.bubble)

    def remaining(self):
        return(display(self.bubble, sort=True, delimeter=', '))

    def pulled(self):
        return(display(self.pulls))

    def pull(self):
        self.mix()
        pull = self.bubble.pop()
        self.pulls.append(pull)
        return pull

    def pause(self, seconds):
        if not self.simulation:
            time.sleep(seconds)

    def message(self, msg):
        if not self.simulation:
            print(msg)

    def results(self):
        if self.item_multiplier is 1:
            return self.pulled()

        item_count = {item: 0 for item in self.items}
        results = []
        for item in self.pulls:
            item_count[item] += 1
            if item_count[item] is self.item_multiplier:
                results.append(item)

        return(display(results))

    def run(self):
        self.message("Here we go...\n")
        self.pause(PAUSE)

        for i in range(len(self.bubble)):
            self.message(f"Pulled: {self.pulled()}")
            self.message(f"Result: {self.results()}\n")
            self.pause(PAUSE)

            self.message('...')
            self.pause(PAUSE_DRAMATIC)
            self.message(f"{self.pull()}\n")
            self.pause(PAUSE_DRAMATIC)

        print(f"Pulled: {self.pulled()}")
        print(f"Result: {self.results()}")

if __name__ == "__main__":
    lottery = Lottery([
        'AMR',
        'GSL',
        'MRN',
        'NRD',
        'NST',
        'TGR',
        'WHL',
    ], 2)
    lottery.run()
